% Funkcja z kazdym obrotem petli tworzy nowa macierz i do niej wpisuje elementy, które sa rozwiazaniem. Macierz L i L* maja takie same elementy wynika to z tego jaka macierz przyjęlismy (dodatnia, symetryczna), więc tworzę tylko jedna
function F=Funkcja(F)
y=size(F); %pobieram informacje o wielkosci macierzy wejsciowej
y=y(1,1); %biorę pierwszy element tej macierzy (macierz jest kwadratowa więc nie ma znaczenia, który element pobiorę)
L = zeros( y, y ); %tworzę macierz o takim samym rozmiarze jak wejsciowa i nie muszę juz tworzyc funkcji, która wypełni zerami elementy powyzej przekatnej głównej
for i=1:y 
   L( i , i ) = sqrt( F ( i , i ) - L ( i , : )*L( i , : )') 
   %wzór dla punktów na przekatnej głównej
   for j = ( i + 1):y
      L( j , i ) = ( F ( j , i ) - L ( i , : )*L( j , :)')/L( i , i ) 
      %wzór dla elementów ponizej przekatnej
   end
end
% Ostatnia macierz L jest macierza z rozwiazaniem zadania
%komentarz komentarz komentarz